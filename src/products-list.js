import { LitElement, html, css } from 'lit-element';

class ProductsList  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        productos: {Array}
    };
  }

  constructor() {
    super();
    this.productos = [];
    this.productos.push({"nombre": "Móvil XL", "description": "Un teléfono grande con una de las mejores pantallas."})
    this.productos.push({"nombre": "Móvil Mini","description": "Un teléfono mediano con una de las mejores cámaras"})
    this.productos.push({"nombre": "Móvil Standard", "description" : "Un teléfono grande con una de las mejores pantallas."})
  }

  render() {
    return html`
      <div class="contenedor">
        ${this.productos.map(p => html `<div class="producto"><h3>${p.nombre}</h3><p>${p.descripcion}</p></div>`)}
    `;
  }

  createRenderRoot(){
      return this;
  }
}

customElements.define('products-list', ProductsList);