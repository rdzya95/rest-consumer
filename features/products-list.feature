Feature: List Products
    Scenario: Cargar lista de productos
        When we request the products list
        Then we should receive
            | nombre | description |
            | Móvil XL | Un teléfono grande con una de las mejores pantallas. |
            | Móvil Mini | Un teléfono mediano con una de las mejores cámaras |
            | Móvil Standard | Un téfono estandar. Nada especial.          | 